# Dockerfile and docker-compose to create LAMP Docker

## Install Docker and docker-compose in your system
### On Ubuntu/Mint
``` 
sudo apt install docker-ce && sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose
```
### On Arch/Manjaro
``` 
sudo pacman -S docker docker-composer
```

## Add your user in docker group
### On Ubuntu/Mint
``` 
usermod -a -G group user
```
### On Arch/Manjaro
``` 
gpasswd -a user group
```

## Start docker
``` 
sudo systemctl start docker
```

## Clone the project
``` 
git clone https://gitlab.com/slowsaz/docker-lamp.git && cd docker-lamp/
```

## Build php image
``` 
docker-compose build
```

## Run the different containers to create LAMP 
``` 
docker-compose up
```
## Manage database
You can manage your database with adminer at localhost:8081.

The access to the database is :

- server : "name_of_db_container"
- username : test or root
- password : password or root

You can test the basic web page at localhost:81 after the database configuration (create table).